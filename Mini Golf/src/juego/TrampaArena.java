package juego;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class TrampaArena {

	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image foto2;

	TrampaArena(double x, double y) {
		this.x=x;
		this.y=y;
		this.alto=46;
		this.ancho=105;
		this.foto2=Herramientas.cargarImagen("imag1.jpg");
	}
	void dibujarTrampaArena(Entorno entorno){
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, alto, 0, Color.yellow);
		entorno.dibujarImagen(foto2, x, y, 0, 0.08);
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;	
		}
	public double getAncho(){
		return this.ancho;
	}
	public double getAlto(){
		return this.alto;
	}

}

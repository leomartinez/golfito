package juego;
	
import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Hoyo {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image foto;
   
	Hoyo (double x, double y){
		this.x=x;
		this.y=y;
		this.alto=25;
		this.ancho=25;
		this.foto= Herramientas.cargarImagen("hoyo.gif");
		}
	void dibujarHoyo(Entorno entorno){
		entorno.dibujarRectangulo(this.x,this.y,this.ancho, this.alto, 0, Color.BLACK);
		entorno.dibujarImagen(this.foto,this.x , this.y, 0, 0.07);
	}
	boolean estaDentroLaPelota(Pelota p){
		if (p.getX()>=izquierdo() && p.getX()<=derecho() && p.getY()<=inferior()  && p.getY()>=superior()){
			return  true;
			}	
		return false;
		}
	 double superior(){
			double partSup=(this.y)-(this.alto/2);
			return partSup;
		}
		 double inferior(){
			double partInf= (this.alto/2)+ this.y;
			return partInf;
		}
		 double derecho(){
			double partDer= (this.ancho/2)+(this.x);
			return partDer;
		}
		 double izquierdo(){
			double partIzq=(this.x)-(this.ancho/2);
			return partIzq;
		}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
		
	}

}

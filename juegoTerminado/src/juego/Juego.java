package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelotita;
	private Hoyo agujero;
	private Flecha flecha;
	private Potencia barrita;
	private TrampaAgua [] agua ;
	private TrampaArena [] arena;
	private Image imag;
	private Image imag1;
	private Image imag2;
	private int contadorGolpes;
	private boolean bandera;
	private Clip sonido;
	private Clip sonido2;
	// Variables y métodos propios de cada grupo
	// ...
	
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Golfito - Grupo Apellido1 - Apellido2 -Apellido3 - V0.01", 800, 600);
		
		this.pelotita= new Pelota(50,50,0,0);
		this.agujero=new Hoyo(750,550);
		this.flecha= new Flecha (pelotita.getX(),pelotita.getY());
		this.barrita= new Potencia(25,580,1);
		this.arena= new TrampaArena[3];
		this.agua= new TrampaAgua [3];
		this.imag= Herramientas.cargarImagen("pasto2.jpg");
		this.sonido=Herramientas.cargarSonido("Game-of-thrones.wav");
		this.imag1=Herramientas.cargarImagen("fuegos.gif");
		this.imag2=Herramientas.cargarImagen("fuegos2.gif");
		this.sonido2=Herramientas.cargarSonido("aplausos.wav");
		this.contadorGolpes=-1;
		this.bandera=true;
		crearTrampaArena(arena);
		crearTrampaAgua(agua);
		 
		// Inicializar lo que haga falta para el juego
		// ...

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
	 this.sonido.start();
	 this.sonido.loop(this.sonido.LOOP_CONTINUOUSLY);
	 String golpes= String.valueOf(this.contadorGolpes);
	 entorno.dibujarImagen(this.imag, 400, 300, 0, 1);
	 entorno.cambiarFont("Times New Roman", 30,Color.white);
	 entorno.escribirTexto("golpes: "+golpes, 680, 40);
	 if(!agujero.estaDentroLaPelota(pelotita)){
		 	 
		 	 dibujarTrampaArena(arena);
			 dibujarTrampaAgua(agua);
			//movimiento de la flecha 
			 if (entorno.estaPresionada(entorno.TECLA_DERECHA)){
				 flecha.moverFlechaDer();
			 }
			 if  (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)){
				flecha.moverFlechaIzq(); 
			 }
			  // la pelota esta en movimiento y se frenara gradualmente.
			 
		 	
			if (entorno.sePresiono(entorno.TECLA_ESPACIO) || entorno.estaPresionada(entorno.TECLA_ESPACIO)){
				 barrita.dibujarBarra(entorno);
				 barrita.aumentarPotencia();
				 pelotita.setAngulo(flecha.getAngulo());
				 pelotita.setVelocidad(barrita.getAlto()/2);
				 this.bandera=true;
				 
			 }
			else{
				this.pelotita.mover();
				this.barrita.setAlto(0);
			}
			this.agujero.dibujarHoyo(entorno);
			
			
			if(this.pelotita.getVelocidad()==0 && this.bandera ) {
				this.contadorGolpes++;
				this.bandera=false;
				System.out.println(this.contadorGolpes);
				
				
			}
			
			this.pelotita.dibujarPelota(entorno);
				
			
			if(pelotita.estaEnMovimiento(pelotita,entorno)){
				this.pelotita.frenar();
				if(pelotita.tocaPared(pelotita,entorno)){
			 		this.pelotita.cambiarAngulo();
			 		this.pelotita.mover();
			 	}
			}
			else{
				this.pelotita.setVelocidad(0);
				this.flecha.setX(this.pelotita.getX());
				this.flecha.setY(this.pelotita.getY());
				this.flecha.dibujar(entorno);
				
			}
			
			if(estaEnTrampaArena(pelotita,arena)){
				if (this.pelotita.getVelocidad()>0)
					this.pelotita.setVelocidad(this.pelotita.getVelocidad()-0.1);
			}
			if(estaEnTrampaAgua(pelotita,agua)){
				this.pelotita.setVelocidad(-5);
				this.contadorGolpes++;
			}
	 }
	 
	 else if(bandera) {
		 this.contadorGolpes++;
		 this.bandera=false;
	 }
	 else{	
		 this.sonido.close();
		 this.sonido2.loop(this.sonido2.LOOP_CONTINUOUSLY);
		 entorno.cambiarFont("Times New Roman", 50, Color.black);
	  	 entorno.escribirTexto("Ganaste!!", 350, 300);
		 entorno.dibujarImagen(imag1, 300, 309, 0, 1);
		 entorno.dibujarImagen(imag2, 600, 306, 0, 1);
	 }
}

	
	//-----------------------------------------------------//

	private void crearTrampaArena(TrampaArena [] arena){
		Random rand= new Random();
		int recordarY=0;
		for (int i = 0; i < arena.length; i++) {
			int posicionX= rand.nextInt(700);
			int posicionY= rand.nextInt(600);
			if(posicionY!= recordarY){
				arena[i]= new TrampaArena(posicionX,posicionY);
				recordarY=posicionY;
			}
		}
	}
	private void crearTrampaAgua(TrampaAgua[] agua){
		Random rand=new Random();
		int recordarY=0; 
		for (int i = 0; i < agua.length; i++) {
			int posicionX=rand.nextInt(700);
			int posicionY=rand.nextInt(600);
			if(posicionY!=recordarY){
				agua[i]=new TrampaAgua (posicionX,posicionY);
				recordarY=posicionY;
			}
		}	
	}
	
	private void dibujarTrampaArena(TrampaArena [] arena){
		for (int i = 0; i < arena.length; i++) {
			if (arena[i]!=null){
				arena[i].dibujarTrampaArena(entorno);
			}
		}
	}
	private void dibujarTrampaAgua(TrampaAgua []agua){
		for (int i = 0; i < agua.length; i++) {
			if (agua[i]!= null){
				agua[i].dibujarTrampaAgua(entorno);
			}
		}
	}
	 boolean estaEnTrampaArena(Pelota p,TrampaArena[] trampa){
		for (int i = 0; i < trampa.length; i++) {
		if(trampa[i]!= null){
			if (p.getX()>=trampa[i].izquierdoArena() && p.getX()<=trampa[i].derechoArena())
				if( p.getY()<=trampa[i].inferiorArena()  && p.getY()>=trampa[i].superiorArena())
						return  true;
			}
		}
		return false;
	 }
	boolean estaEnTrampaAgua(Pelota p,TrampaAgua[] trampa){
		for (int i = 0; i < trampa.length; i++){
		  if(trampa[i]!= null)
			if (p.getX()>=trampa[i].izquierdoAgua() && p.getX()<=trampa[i].derechoAgua())
				if( p.getY()<=trampa[i].inferiorAgua()  && p.getY()>=trampa[i].superiorAgua())
						return  true;
			}
		return false;
	}
	

	
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}

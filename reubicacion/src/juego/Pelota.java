package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {
	private double x;
	private double y;
	private double angulo;
	private double velocidad;
	private Image foto3;
   
	Pelota (double x, double y,double angulo, double velocidad){
		this.x=x;
		this.y=y;
		this.angulo =angulo;
		this.velocidad=velocidad;
		this.foto3 = Herramientas.cargarImagen("51193.gif");
		
	}
	void dibujarPelota(Entorno entorno){
		entorno.dibujarCirculo(this.x, this.y, 15, Color.white);
		entorno.dibujarImagen(foto3, x, y, 0 , 0.2);
	}
	void mover(){
		this.x += this.velocidad*Math.cos(this.angulo);
		this.y += this.velocidad*Math.sin(this.angulo);
	}
	void frenar(){
		this.velocidad=this.velocidad-0.02;
	}
	void frenarPorTrampa(){
		this.velocidad+=0.12;
	}

	void cambiarAngulo(){
		this.angulo+=Math.PI/2;
	}
	boolean tocaPared(Pelota p,Entorno entorno){
		return p.getX()>entorno.ancho()-5 || p.getX() <5 || p.getY()<5 || p.getY()>entorno.alto()-15;
	}
	boolean estaEnMovimiento(Pelota p,Entorno entorno){
		if( p.getVelocidad()>0)
			return true;
		return false;
	}
	public double getAngulo(){
		return this.angulo;
	}
	public double getVelocidad(){
		return this.velocidad;
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public void setVelocidad(double velocidad){
		this.velocidad=velocidad;
	}
	public void setAngulo(double angulo){
		this.angulo=angulo;
	}
	public void setX(double x){
		this.x=x;
	}
	public void setY(double y){
		this.x=y;
	}

}
